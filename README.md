# Techflitter

Practical Task

Please follow steps to setup this project,

1) Install composer using "composer install" command.
2) Create environment(.env) file and configure it.
3) Run this "php artisan key:generate" command to generate key of that project.
4) Run these below commands,
    - php artisan route:clear
    - php artisan config:cache
    - php artisan view:clear.
    - php artisan config:cache.
5) Create databse and mention this database details into .env file.
6) Run this "php artisan migrate" command for migration database tables.
7) To get random records of users(50,000) run seeder command "php artisan migrate".

Note:- All users have same default password to login and that is "password".