<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Redirect,Response;
use Yajra\Datatables\Datatables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
        	$output = array();
        	$datatable_fields = array('id', 'name','email');
            $data = User::orderBy('updated_at','DESC');
            if ($request['search']['value'] != '') {
	            $data->where(function($query) use ($request, $datatable_fields) {
	                for ($i = 0; $i < count($datatable_fields); $i++) {
	                    if ($request['columns'][$i]['searchable'] == true) {
	                        $query->orWhere($datatable_fields[$i], 'like', '%' . $request['search']['value'] . '%');
	                    }
	                }
	            });
	        }
            $count = $data->count();
            $data->skip($request['start'])->take($request['length']);
            $output['recordsTotal'] = $count;
	        $output['recordsFiltered'] = $count;
	        $output['draw'] = $request['draw'];
	        $output['data'] = $data->get();
	        return json_encode($output);
        }
      
        return view('dashboard');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        User::updateOrCreate(['id' => $request->user_id],
                ['name' => $request->name, 'email' => $request->email, 'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi']);        
   
        return response()->json(['success'=>'User saved successfully.']);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::find($id)->delete();
     
        return response()->json(['success'=>'User deleted successfully.']);
    }
}
